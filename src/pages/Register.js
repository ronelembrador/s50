import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import { Redirect, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register() {

	const history = useHistory()

	// State hooks to store the values of the input values
	const { user } = useContext(UserContext)
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')
	const [fname, setFname] = useState('')
	const [lname, setLname] = useState('')
	const [mobile, setMobile] = useState('')
	const [isError, setIsError] = useState(false)

	// State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	// console.log(email)
	// console.log(password1)
	// console.log(password2)
	// console.log(fname)
	// console.log(lname)
	// console.log(mobile)
	// console.log(isError)

	function registerUser(e){

			// console.log(data)
           	console.log(email)
			console.log(password1)
			console.log(password2)
			console.log(fname)
			console.log(lname)
			console.log(mobile)
			console.log(isError)

		console.log(e.target.value)
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			body: JSON.stringify({
				email: email
			}),
			headers: {
				'Content-Type' : 'application/json'
			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			if(data){
				Swal.fire({
					title: 'Duplicate Email',
					icon: 'error',
					text: 'Email already registered. Please change email'
				})
			} else {
				fetch(`http://localhost:4000/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: fname,
                        lastName: lname,
                        email: email,
                        mobileNo: mobile,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data){

                        // Clear input fields
                        setFname('');
                        setLname('');
                        setEmail('');
                        setMobile('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Zuitt!'
                        });

						history.push("/login")
					} else {
						Swal.fire({
                            title: 'Error encountered',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

					}
				})
			}
		})

		// // Prevents page redirection via form submission
		// e.preventDefault();

		// // clears the input fields
		// setFname('')
		// setLname('')
		// setMobile(0)
		// setEmail('')
		// setPassword1('')
		// setPassword2('')
		// setIsError(false)

		// alert('Thank you for registering!')
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if((fname !== '' && lname !== '' && mobile.length >= 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password1, password2, lname, fname, mobile])

	return(

	(user.id !== null) ?
		<Redirect to="/courses" />
		:
		<Form onSubmit = {(e) => registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type= 'text'
					placeholder= 'Please enter your first name here'
					value= {fname}
					onChange= {e => setFname(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type= 'text'
					placeholder= 'Please enter your last name here'
					value= {lname}
					onChange= {e => setLname(e.target.value)}
					required
				/>
			</Form.Group>

			{ isError ?
				<Form.Group className = "text-danger">
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control
						type= 'text'
						placeholder= 'Please enter your mobile number here'
						value= {mobile}
						onChange= {e => {
							setMobile(e.target.value);
							if (e.target.value.length < 11) {
								setIsError(true)
							} else {
								setIsError(false)
							}

						}}
						required
					/>
					<Form.Text className = "text-muted">
						Please input a minimum of 11 numbers
					</Form.Text>
				</Form.Group>

			  :

				  <Form.Group className = "text-dark">
						<Form.Label>Mobile Number:</Form.Label>
						<Form.Control
							type= 'text'
							placeholder= 'Please enter your mobile number here'
							value= {mobile}
							onChange= {e => {
								setMobile(e.target.value);
								if (e.target.value.length < 11) {
									setIsError(true)
								} else {
									setIsError(false)
								}

							}}
							required
						/>
						<Form.Text className = "text-muted" hidden>
						Please input a minimum of 11 numbers
						</Form.Text>
					</Form.Group>
			}

			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type= 'email'
					placeholder= 'Please enter your email here'
					value= {email}
					onChange= {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className = "text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type = 'password'
					placeholder = 'Please input your password here'
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId = "password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type = 'password'
					placeholder = 'Please verify your password'
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'>
					Register
				</Button>

				:
				<Button variant = 'danger' type = 'submit' id = 'submitBtn' disabled>
					Register
				</Button>
			}
		</Form>

	)
}


/* Ternary Button with the Button Tag

?- if -- if <Button isActive, the color will be primary and it is enabled>
:- else -- else <Button will be color danger and will be disabled>*/

/*useEffect - react hook 
Syntax */