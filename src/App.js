//import {Fragment} from 'react';
import { useState, useEffect } from 'react'
import {Container} from 'react-bootstrap';
import AppNavBar from './components/AppNavBar'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses';
import CourseView from './pages/CourseView'
import NotFound from './pages/NotFound';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';
import { UserProvider } from './UserContext'

function App() {

  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the information and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage
  const unsetUser = () => {
    localStorage.clear()
  }


  useEffect(() => {
    let token = localStorage.getItem('token')
    fetch('http://localhost:4000/users/details', {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // console.log(data)

      if(typeof data._id !== "undefined"){
        setUser({
          id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
    // console.log(user)
    // console.log(localStorage)
  }, [])

  // The UserProvider component is what allows other components to consume or use our context. Any component which is not wrapped by UserProvider will not have access to the values provided for our context

  // You can pass data or information to our context by providing a "value" attribute in our UserProvider. Data passed here can be access by other components by unwrapping our context using the useContext hook.

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/courses/:courseId" component={CourseView} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/logout" component={Logout} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;


/*
  
  ReactJS is a single page application(SPA). However, we can simulate the changing of pages. We don't actually create new pages, what we just do is switch pages according to their assigned routes. ReactJS and react-router-dom package just mimics or mirrors how HTML access its URL.


  react-router-dom 3 main components to simulate the changing of page

  1. Router - wrapping the router component around other components will allow us to use routing within our page.
  2. Switch - Allow us to switch/ change our page components
  3. Route - assigns a path which will trigger the change/switch of components render.

*/
